import pycuda.autoinit
import pycuda.driver as drv
import numpy as np
from pycuda.compiler import SourceModule
import time
mod = SourceModule(open("pi.cu").read())
divide = mod.get_function("divide")
suma = mod.get_function("sum")

a = np.ones((1000,1000), dtype=np.float32) * 4
b = np.arange(1,2000000,2, dtype=np.float32)
b = np.reshape(b,(1000,1000))
divided = np.empty_like(a)

result_pi = np.empty(1000, dtype=np.float32)
final_pi = np.empty(1, dtype=np.float32)

divide(drv.Out(divided), drv.In(a), drv.In(b), block=(100,10,1), grid=(1000,1))
#print(sum(sum(divided)))
suma(drv.Out(result_pi), drv.In(divided), block=(1000,1,1), grid=(1000,1))
print(sum(result_pi))

suma(drv.Out(final_pi), drv.In(result_pi), block=(1000,1,1), grid=(1,1))

print (final_pi)
