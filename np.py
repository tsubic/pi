import numpy as np


a = np.array([[9, 13, 5],
              [3, 7, 2],
              [6, 0, 7]], dtype=np.float32)
b = np.array([[1, 2, 3], [5, 6, 7], [9, 10, 11]], dtype=np.float64)

c = 2*a+b;

d = np.cos(a)

print(c)
print(d)
print(np.sqrt(b))
