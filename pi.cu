#include<stdio.h>

__global__ void divide(float *dest, float *a, float *b)
{
  const int i = threadIdx.x + threadIdx.y * blockDim.x + blockIdx.x * gridDim.x;
  float j = i;
  float smn = -1;
  dest[i] = pow(smn,j) * a[i] / b[i];
}

__global__ void sum(float *dest, float *src)
{
  __shared__ float cache[1000];

  const int idx = threadIdx.x + blockDim.x * blockIdx.x;
  const int tidx = threadIdx.x;
 
  cache[tidx] = src[idx];
  int active = blockDim.x / 2;

  do
    {
      __syncthreads();
      if (tidx < active)
        {
          cache[tidx] += cache[tidx + active];
        }
      active /= 2;
    }
  while (active > 0);

  if (tidx == 0)
    {
      dest[blockIdx.x] = cache[0];
    }
}
